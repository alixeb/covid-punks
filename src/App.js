import React from 'react';
import { contract } from './contract';

const getContractData = async function(params = {}) {
  const { start = 1, end = 100 } = params;
  const data = [];

  // console.log(await contract.name());
  // const totalSupply = await contract.totalSupply();
  // console.log(`totalSupply = ${totalSupply.toString()}`);
  
  // console.log(await contract.tokenByIndex(1)); // <-- this fails o_O
  // console.log(await contract.tokenURI(6076)); // <-- this fails o_O
  // console.log(await contract.ownerOf(6076)); // <-- this fails o_O

  /* for loop commented out because of error 
  for (let i = start; i <= end; i++) {
    const tokenId = await contract.tokenByIndex(i);
    const uri = await contract.tokenURI(tokenId);
    const owner = await contract.ownerOf(tokenId);
    data.push({ tokenId, uri, owner });
  }*/

  return data;
};

const getIpfsData = async function(data) {
  const metaData = [];

  for (let i = 0; i < data.length; i++) {
    const { uri, owner } = data[i]
    const response = await fetch(uri);
    const json = await response.json();
    metaData.push({ ...json, owner });
  }

  return metaData;
}

const mockData = [
  { tokenId: 6076, uri: 'https://gateway.pinata.cloud/ipfs/QmcrxZz2ZhMST6Lt2evx44dtTjUq34gejRhUAQSqhZmktb/6076.json', owner: '0x6c11429E514a5a53749F7F36D3dA36434EE8D62d' },
  { tokenId: 4119, uri: 'https://gateway.pinata.cloud/ipfs/QmY4cCT4H8ndroZw4mPVG4C4knDJ3MDoxvSfHV4ghjx2bo/4119.json', owner: '0xDD16442029D9C9DC337bA7D5c0DFee54AcF6fA29' },
  { tokenId: 2422, uri: 'https://gateway.pinata.cloud/ipfs/QmY4cCT4H8ndroZw4mPVG4C4knDJ3MDoxvSfHV4ghjx2bo/2422.json', owner: '0xB3dD1EC6DE165273Da2aa98f672a8f387C9FD51B' },
  { tokenId: 7062, uri: 'https://gateway.pinata.cloud/ipfs/QmY4cCT4H8ndroZw4mPVG4C4knDJ3MDoxvSfHV4ghjx2bo/7062.json', owner: '0x85c8B84498363F4AEd187E5be7A1d9d8eeA2a378' },
  { tokenId: 1006, uri: 'https://gateway.pinata.cloud/ipfs/QmY4cCT4H8ndroZw4mPVG4C4knDJ3MDoxvSfHV4ghjx2bo/1006.json', owner: '0x01Cd7ACA917E412130DD8EeD86040540F19B81BD' },
  { tokenId: 7270, uri: 'https://gateway.pinata.cloud/ipfs/QmY4cCT4H8ndroZw4mPVG4C4knDJ3MDoxvSfHV4ghjx2bo/7270.json', owner: '0x418eCef2F34Bf57C658bd5e249e946482391D933' },
  { tokenId: 1068, uri: 'https://gateway.pinata.cloud/ipfs/QmY4cCT4H8ndroZw4mPVG4C4knDJ3MDoxvSfHV4ghjx2bo/1068.json', owner: '0xcEb7C97B39725844aE95c61480812F96004581B9' }
]

class App extends React.Component {
  state = {
    metaData: []
  };

  componentDidMount() {
    this.fetchData();
  }

  fetchData = async() => {
    const data = await getContractData();
    
    // using mockData retrieved manually via etherscan.io
    // https://etherscan.io/address/0xe4cfae3aa41115cb94cff39bb5dbae8bd0ea9d41#readContract
    const metaData = await getIpfsData(mockData);
    
    this.setState({ metaData })
  }

  render() {
    const { metaData } = this.state;

    return (
      <div className="App">
        <h1>
          COVIDPunks!
        </h1>
        <div className="crypto-punk-container">
          { 
            metaData.map((item) => {
              const { image, name, tokenId, owner } = item;
              return (
                <div key={tokenId} className="crypto-punk">
                  <img src={image} alt={name} title={name} />
                  <p>{name}</p>
                  <p>{owner}</p>
                </div>
              )
            })
          }
        </div>
      </div>
    );
  }
}

export default App;
