import { render, screen } from '@testing-library/react';
import App from './App';

test('renders crypto punk text', () => {
  render(<App />);
  const element = screen.getByText(/crypto punk/i);
  expect(element).toBeInTheDocument();
});
